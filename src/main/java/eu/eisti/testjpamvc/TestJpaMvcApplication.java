package eu.eisti.testjpamvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestJpaMvcApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestJpaMvcApplication.class, args);
    }
}
