package eu.eisti.testjpamvc.init;

import eu.eisti.testjpamvc.entities.User;
import eu.eisti.testjpamvc.models.UserCommand;
import eu.eisti.testjpamvc.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Stream;

@Component
@Profile("init")
public class UserDatabaseInit {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    void init() {
        var users = Stream.of(
                new UserCommand("Florent"),
                new UserCommand("Adrien"),
                new UserCommand("Jean-Baptiste"),
                new UserCommand("Maxime")
        ).map(User::new);
        userRepository.saveAll(users.toList());
    }
}
