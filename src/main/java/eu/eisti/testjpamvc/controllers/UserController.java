package eu.eisti.testjpamvc.controllers;

import eu.eisti.testjpamvc.models.UserCommand;
import eu.eisti.testjpamvc.models.dto.UserDTO;
import eu.eisti.testjpamvc.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        return ResponseEntity
                .ok()
                .body(userService.findAll());
    }

    @GetMapping(params = "letter")
    public ResponseEntity<List<UserDTO>> getAllUsersStartingWith(@RequestParam String letter) {
        return ResponseEntity
                .ok(userService.findAllUsersStartingWith(letter));
    }

    @PostMapping()
    public ResponseEntity<UserDTO> addUser(@RequestBody UserCommand command) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(userService.addUser(command));
    }

    @GetMapping("{id}")
    public ResponseEntity<UserDTO> findUserById(@PathVariable UUID id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<UserDTO> deleteUserById(@PathVariable UUID id) {
        return ResponseEntity.ok(userService.deleteById(id));
    }
}
