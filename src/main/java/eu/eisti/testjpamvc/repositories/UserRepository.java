package eu.eisti.testjpamvc.repositories;

import eu.eisti.testjpamvc.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findByUsernameStartingWith(String letter);
}
