package eu.eisti.testjpamvc.models.dto;

import eu.eisti.testjpamvc.entities.User;

import java.util.UUID;

public record UserDTO(UUID id, String name) {
    public static UserDTO fromUser(User user) {
        return new UserDTO(user.getId(), user.getUsername());
    }
}
