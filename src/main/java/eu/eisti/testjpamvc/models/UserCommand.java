package eu.eisti.testjpamvc.models;

public record UserCommand(String name) {
}
