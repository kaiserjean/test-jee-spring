package eu.eisti.testjpamvc.services;

import eu.eisti.testjpamvc.entities.User;
import eu.eisti.testjpamvc.models.UserCommand;
import eu.eisti.testjpamvc.models.dto.UserDTO;
import eu.eisti.testjpamvc.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDTO> findAll() {
        return userRepository.findAll()
                .stream().map(UserDTO::fromUser)
                .toList();
    }

    public UserDTO addUser(UserCommand command) {
        return UserDTO.fromUser(userRepository.save(new User(command)));
    }

    public UserDTO findById(UUID id) {
        return UserDTO.fromUser(userRepository.getById(id));
    }

    public UserDTO deleteById(UUID id) {
        var deleted = userRepository.findById(id);
        userRepository.deleteById(id);
        return UserDTO.fromUser(deleted.orElseThrow());
    }

    public List<UserDTO> findAllUsersStartingWith(String letter) {
        return userRepository.findByUsernameStartingWith(letter)
                .stream().map(UserDTO::fromUser)
                .toList();
    }
}
